FROM nginx:alpine

ADD https://gitlab.com/jholzapfel/nginx-server/-/raw/master/Views/index.html /usr/share/nginx/html/

RUN chmod +r /usr/share/nginx/html/index.html

CMD ["nginx", "-g", "daemon off;"]